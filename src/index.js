const express = require('express');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const mongoose = require('mongoose');
const userRoutes = require('./routes/userRoutes');
const vinyleRoutes = require('./routes/vinyleRoutes');
const albumRoutes = require('./routes/albumRoutes');

const app = express();
const ads = [{ title: 'Hello, world (again)!' }];

app.use(helmet());
app.use(express.json());
app.use(cors());
app.use(morgan('combined'));

// Connect to MongoDB
mongoose
  .connect(
    'mongodb+srv://groovy:WAap90kSVkJfO64M@cluster0.llp9m.mongodb.net/groovy?retryWrites=true&w=majority',
    { useNewUrlParser: true }
  )
  .then(() => {
    console.log('Connected to MongoDB');

    // route middlewares
    app.use('/api/users', userRoutes);
    app.use('/api/vinyles', vinyleRoutes);
    app.use('/api/albums', albumRoutes);

    app.get('/', (req, res) => {
      res.send(ads);
    });

    app.listen(8080, () => {
      console.log('listening on port 3001');
    });
  })
  .catch((err) => {
    console.log(err);
  });
