// This app is a marketplace for vinyle, there is pro and non pro sellers
const Vinyle = require('../models/vinyleModel');
const User = require('../models/userModel');
const Album = require('../models/albumModel');
const Artist = require('../models/artistModel');
const spotify = require('./spotifyController');

exports.getAll = async (req, res) => {
  try {
    const vinyls = await Vinyle.find({});
    res.send(vinyls);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
};

exports.getOne = async (req, res) => {
  try {
    const vinyle = await Vinyle.findById(req.params.id);
    if (!vinyle) {
      return res.status(404).send({ error: 'Vinyle not found' });
    }
    res.send(vinyle);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
};

exports.createOne = async (req, res) => {
  try {
    spotify.search(req.body.artist, req.body.title).then(async (data) => {
      const { artist, album } = data;

      let findArtist = await Artist.findOne({ spotifyId: artist.spotifyId });

      if (!findArtist) {
        const newArtist = new Artist(artist);
        await newArtist.save();
        findArtist = newArtist;
      }

      let findAlbum = await Album.findOne({ spotifyId: album.spotifyId });

      if (!findAlbum) {
        const newAlbum = new Album({
          ...album,
          artist: findArtist._id,
        });
        await newAlbum.save();
        findAlbum = newAlbum;
      }

      const newVinyle = new Vinyle({
        ...req.body,
        album: findAlbum._id,
        condition: 'New',
      });

      await newVinyle.save();

      res.status(201).send(newVinyle);
    });
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
};

exports.updateOne = async (req, res) => {
  try {
    const vinyle = await Vinyle.findByIdAndUpdate(req.params._id, req.body, {
      new: true,
    });
    if (!vinyle) {
      return res.status(404).send({ error: 'Vinyle not found' });
    }
    res.send(vinyle);
  } catch (error) {
    res.status(400).send({ error: error.message });
  }
};

exports.deleteOne = async (req, res) => {
  try {
    const vinyle = await Vinyle.findByIdAndDelete(req.params.id);
    if (!vinyle) {
      return res.status(404).send({ error: 'Vinyle not found' });
    }
    res.send(vinyle);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
};

exports.getAllVinylesByUser = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    if (!user) {
      return res.status(404).send({ error: 'User not found' });
    }
    await user.populate('vinyles').execPopulate();
    res.send(user.vinyles);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
};

exports.addProposition = async (req, res) => {
  try {
    const vinyle = await Vinyle.findById(req.params.id);
    if (!vinyle) {
      return res.status(404).send({ error: 'Vinyle not found' });
    }
    vinyle.propositions.push(req.body);
    await vinyle.save();
    res.send(vinyle);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
};

exports.deleteProposition = async (req, res) => {
  try {
    const vinyle = await Vinyle.findById(req.params.id);
    if (!vinyle) {
      return res.status(404).send({ error: 'Vinyle not found' });
    }
    vinyle.propositions = vinyle.propositions.filter(
      (proposition) => proposition._id !== req.params.propositionId
    );
    await vinyle.save();
    res.send(vinyle);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
};

exports.acceptProposition = async (req, res) => {
  try {
    const vinyle = await Vinyle.findById(req.params.id);
    if (!vinyle) {
      return res.status(404).send({ error: 'Vinyle not found' });
    }
    vinyle.propositions = vinyle.propositions.map((proposition) =>
      proposition._id === req.params.propositionId
        ? { ...proposition, status: 'accepted' }
        : proposition
    );
    await vinyle.save();
    res.send(vinyle);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
};

exports.declineProposition = async (req, res) => {
  try {
    const vinyle = await Vinyle.findById(req.params.id);
    if (!vinyle) {
      return res.status(404).send({ error: 'Vinyle not found' });
    }
    vinyle.propositions = vinyle.propositions.map((proposition) =>
      proposition._id === req.params.propositionId
        ? { ...proposition, status: 'declined' }
        : proposition
    );
    await vinyle.save();
    res.send(vinyle);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
};
