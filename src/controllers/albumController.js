const Album = require('../models/albumModel');
const Vinyle = require('../models/vinyleModel');
const Artist = require('../models/artistModel');
const User = require('../models/userModel');

exports.getAll = async (req, res) => {
  try {
    // Get all vinyls grouped by album with populated album and artist
    // populate seller
    let vinyls = await Vinyle.aggregate([
      {
        $group: {
          _id: '$album',
          count: { $sum: 1 },
          minPrice: { $min: '$price' },
          maxPrice: { $max: '$price' },
          offers: { $push: '$$ROOT' },
        },
      },
      {
        $lookup: {
          from: 'albums',
          localField: '_id',
          foreignField: '_id',
          as: 'album',
        },
      },
      {
        $unwind: '$album',
      },
      {
        $lookup: {
          from: 'artists',
          localField: 'album.artist',
          foreignField: '_id',
          as: 'album.artist',
        },
      },
      {
        $unwind: '$album.artist',
      },

    ]);

    console.log(vinyls);
    const users = await User.find({}, '-tokens');

    vinyls = vinyls.map((vinyle) => {
      vinyle.offers = vinyle.offers.map((offer) => {
        offer.seller = users.find((user) => user._id.toString() === offer.seller.toString());
        return offer;
      });
      return vinyle;
     }).sort((a, b) => b.count - a.count);


    res.status(200).send(vinyls);
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
};
