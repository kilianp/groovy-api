const SpotifyWebApi = require('spotify-web-api-node');
const moment = require('moment');
const axios = require('axios');

const spotifyUserId = 'phz163sjppy093wdlu7hmx3rd'; // Prams' account

const spotifyApi = new SpotifyWebApi({
  clientId: '83155373eeca4cf185111d9f742efdf4',
  clientSecret: 'd149f6f86996415aa0b8263fcda42a29',
  redirectUri: 'http://localhost:8888/callback',
  refreshToken:
    'AQB5cMYyoBB_NrsqPR_5_1iOuOsunIBz7lFQN5VZJ-wu1MdORvCI9dnc7sR4UYByjKdlO-RLfm1ITeEUvwvhTWpYnUehW5eujbJ9CvPPPTYi4jlpBGiarJyAUX0mV9yGntE',
});

const refreshAccessToken = async () => {
  console.log('ok');
  const result = await spotifyApi.refreshAccessToken();
  this.dateExpire = moment().add(result.body.expires_in, 'seconds').toDate();

  spotifyApi.setAccessToken(result.body.access_token);
  this.axiosClient = axios.create({
    headers: { Authorization: `Bearer ${result.body.access_token}` },
  });
};

const search = async (artist, albumName) =>
  refreshAccessToken().then(() =>
    spotifyApi.searchArtists(artist).then(
      (artistData) => {
        let spotifyArtist = artistData.body.artists.items[0];

        spotifyArtist = {
          spotifyId: spotifyArtist.id,
          name: spotifyArtist.name,
          image: spotifyArtist.images[0].url,
          genres: spotifyArtist.genres,
          popularity: spotifyArtist.popularity,
        };

        return spotifyApi.getArtistAlbums(spotifyArtist.spotifyId).then(
          (albumData) => {
            let spotifyAlbum = albumData.body.items.find(
              (album) => album.name.toUpperCase() === albumName.toUpperCase()
            );

            spotifyAlbum = {
              name: spotifyAlbum.name,
              spotifyId: spotifyAlbum.id,
              releaseDate: spotifyAlbum.release_date,
              cover: spotifyAlbum.images[0].url,
              totalTracks: spotifyAlbum.total_tracks,
            };

            return spotifyApi.getAlbumTracks(spotifyAlbum.spotifyId).then(
              (tracksData) => {
                const spotifyTracks = tracksData.body.items.map((track) => ({
                  name: track.name,
                  duration: track.duration_ms,
                  track_number: track.track_number,
                  spotifyId: track.id,
                }));

                const returnData = {
                  artist: spotifyArtist,
                  album: {
                    ...spotifyAlbum,
                    tracks: spotifyTracks,
                  },
                };

                return returnData;
              },
              (err) => {
                console.log('Something went wrong!', err);
              }
            );
          },
          (err) => {
            console.error(err);
          }
        );
      },
      (err) => {
        console.error(err);
      }
    )
  );
// Search artists whose name contains 'Love'

module.exports = {
  refreshAccessToken,
  search,
};
