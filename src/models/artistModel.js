const mongoose = require('mongoose');

const { Schema } = mongoose;

const artistSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  spotifyId: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  genres: {
    type: Array,
    required: true,
  },
  popularity: {
    type: Number,
    required: true,
  },
});

artistSchema.set('toJSON', {
  transform: (doc, ret) => {
    delete ret.__v;
    return ret;
  },
});

module.exports = mongoose.model('Artist', artistSchema);
