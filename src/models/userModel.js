// Basic user mongoDb model
const mongoose = require('mongoose');

const { Schema } = mongoose;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('../config');

const UserSchema = new Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },

  password: {
    type: String,
    required: true,
  },

  tokens: [
    {
      token: {
        type: String,
        required: true,
      },
    },
  ],

  firstname: {
    type: String,
    required: true,
  },

  lastname: {
    type: String,
    required: true,
  },

  address: {
    type: String,
    required: true,
  },

  isPro: {
    type: Boolean,
    required: true,
    default: false,
  },

  role: {
    type: String,
    required: true,
    in: ['admin', 'user'],
    default: 'user',
  },

  avatar: {
    type: String,
    required: false,
  },
});

UserSchema.methods.generateAuthToken = async function () {
  const user = this;
  const token = jwt.sign(
    {
      _id: user._id.toString(),
      email: user.email,
      firstname: user.firstname,
      lastname: user.lastname,
      address: user.address,
      isPro: user.isPro,
    },
    config.JWT_SECRET
  );

  // Add token to user but 5 max

  user.tokens = user.tokens.concat({ token });
  if (user.tokens.length > 5) {
    user.tokens.shift();
  }

  await user.save();

  return token;
};

UserSchema.pre(
  'save',
  async function (next) {
    const user = this;

    if (user.isModified('password')) {
      user.password = await bcrypt.hash(user.password, 8);
    }

    next();
  },
  (error) => {
    console.log(error);
  }
);

UserSchema.set('toJSON', {
  transform: (doc, ret) => {
    delete ret.__v;
    return ret;
  },
});

module.exports = mongoose.model('User', UserSchema);
