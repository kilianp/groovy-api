// Sale model, with vinyle id, buyer, and invoice

const mongoose = require('mongoose');

const saleSchema = new mongoose.Schema({
  vinyle: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Vinyle',
    required: true,
    immutable: true,
  },
  buyer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
    immutable: true,
  },
  sellPrice: {
    type: Number,
    required: true,
    immutable: true,
  },
  invoice: {
    type: String,
    required: true,
    immutable: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
    immutable: true,
  },
});

saleSchema.set('toJSON', {
  transform: (doc, ret) => {
    delete ret.__v;
    return ret;
  },
});

const Sale = mongoose.model('Sale', saleSchema);
