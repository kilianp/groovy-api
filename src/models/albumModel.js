const mongoose = require('mongoose');

const { Schema } = mongoose;

const albumSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  spotifyId: {
    type: String,
    required: true,
  },
  artist: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Artist',
    required: true,
  },
  releaseDate: {
    type: String,
    required: true,
  },
  cover: {
    type: String,
    required: true,
  },
  totalTracks: {
    type: Number,
    required: true,
  },
  tracks: {
    type: Array,
    required: true,
  },
});

albumSchema.set('toJSON', {
  transform: (doc, ret) => {
    delete ret.__v;
    return ret;
  },
});

module.exports = mongoose.model('Album', albumSchema);
