// Vinyle sold item
const mongoose = require('mongoose');

const { Schema } = mongoose;

const vinyleSchema = new Schema({
  album: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  condition: {
    type: String,
    required: true,
    in: [
      'New',
      'Near Mint',
      'Very Good Plus',
      'Very Good',
      'Good Plus',
      'Good',
      'Fair',
      'Poor',
    ],
  },
  price: {
    type: Number,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  images: {
    type: Array,
    required: true,
  },
  seller: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  propositions: [
    {
      buyer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
      },
      price: {
        type: Number,
      },
      status: {
        type: String,
        default: 'pending',
        in: ['pending', 'accepted', 'rejected'],
      },
    },
  ],
});

vinyleSchema.set('toJSON', {
  transform: (doc, ret) => {
    delete ret.__v;
    return ret;
  },
});

module.exports = mongoose.model('Vinyle', vinyleSchema);
