const express = require('express');
const vinyleController = require('../controllers/vinyleController');
const userController = require('../controllers/userController');

const router = express.Router();

router.get('/', vinyleController.getAll);
router.get('/:id', vinyleController.getOne);
router.post('/', vinyleController.createOne);
router.put('/:id', vinyleController.updateOne);
router.delete('/:id', vinyleController.deleteOne);
router.get('/user/:id', vinyleController.getAllVinylesByUser);

module.exports = router;
