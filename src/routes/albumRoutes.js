const express = require('express');
const albumController = require('../controllers/albumController');

const router = express.Router();

router.get('/', albumController.getAll);

module.exports = router;
